Bash Grats
==========

A simple way to log gratitude and view daily log count from command line.

## Deploy
* Add `grats` script to your path. I keep mine in `~/my_scripts`
* Ensure that the `grats` script is executable: `chmod u+x grats`
* Add this function to your `~/.bashrc`:

```
function _gratcount() {
    if [ "$(grats -c)" -lt "3" ]; then
        echo -n "\e[0;31mg[$(grats -c)]\e[0m"
    else
        echo -n "\e[0;35mg[$(grats -c)]\e[0m"
    fi
}
```
* Set your preferred location for gratitude file in `~/.bashrc` if this is not specified, the default location is `~/gratitude.txt`

```
export GRATSFILE="<path_to_grats_file>"
```
* Update your command prompt by calling this method in your `PS1` variable, e.g.: `$(_gratcount)`

## Usage
* Add gratitude:
```
grats "message here. Beware using quotes."
```

* Get random from file:
```
grats -r
```

* Get gratitude count for the day
```
grats -c
```

* Get total gratitude count
```
grats -C
```
